# Phil Capstone Project

## Description
The project is a crowdfunding platform, it aims to collect funds from a substantial amount of people, in order to protect lands or any other sort of patrimony on a nation wise scale. the platform and all investements will be handled by a non-profit association.
Ideally, buying a land and redistributing it across all donors is the objective. There's no single owner but rather numerous (unrestricted) share holders.

## General Tasks
* [ ] Navigations bars (Layout)
* [ ] Landing Page (Home)
* [ ] Blog Page
* [ ] Projects Page